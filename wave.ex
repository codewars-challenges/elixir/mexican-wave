defmodule Wave do
  def wave(str), do: start_wave(str)

  def start_wave(""), do: []
  def start_wave(str) do
    spaces = find_blanks(str)
    str_without_blanks = String.replace(str, " ", "")
    1..String.length(str_without_blanks)
    |> Enum.map(&do_the_wave(&1, str_without_blanks))
    |> Enum.map(&insert_blanks(&1, spaces))
  end

  def find_blanks(str) do
    str
    |> String.split("", trim: true)
    |> Enum.with_index()
    |> Enum.filter(fn ({x, _}) -> x == " " end)
    |> Enum.map(fn ({_, y}) -> y end)
  end

  def insert_blanks(str, []), do: str
  def insert_blanks(str, [h | t]) do
    parts = str
            |> String.split_at(h)
            |> Tuple.to_list()
    insert_blanks(Enum.join(parts, " "), t)
  end


  defp do_the_wave(1, str) do
    wave = str
           |> String.slice(0..0)
           |> String.upcase()
    last = str
           |> String.slice(1..-1)
    "#{wave}#{last}"
  end

  defp do_the_wave(num, str) do
    first = str
            |> String.slice(0..num - 2)
    wave = str
           |> String.slice(num - 1..num - 1)
           |> String.upcase()
    last = str
           |> String.slice(num..-1)
    "#{first}#{wave}#{last}"
  end

end

wave = &Wave.wave/1
IO.inspect wave.("hello")
IO.inspect wave.("")